import aniso8601
import requests
from flask import current_app


class BlogPost(object):
    def __init__(self):
        self.title = ''
        self.create_date = None
        self.slug = ''
        self.tags = []
        self.content_blocks = []


class TextBlock(object):
    def __init__(self, text):
        self.block_type = 'text'
        self.text = text


class ImageBlock(object):
    def __init__(self, url, caption):
        self.block_type = 'image'
        self.url = url
        self.caption = caption


def add_content_block(blog_post, block_data):
    content_block = None
    block_type = block_data['component']
    if block_type == 'textBlock':
        text = block_data['text']
        content_block = TextBlock(text)
    elif block_type == 'imageBlock':
        url = block_data['image']['filename']
        caption = block_data['caption']
        content_block = ImageBlock(url, caption)
    if content_block:
        blog_post.content_blocks.append(content_block)


def parse_response(data):
    blog_posts = []
    for story in data['stories']:
        blog_post = BlogPost()
        blog_post.title = story['content']['title']
        create_date = story['content']['createDate'].split()[0]
        blog_post.slug = story['slug']
        blog_post.create_date = aniso8601.parse_date(create_date)
        blog_post.tags = story['tag_list']
        for block_data in story['content']['contentBlocks']:
            add_content_block(blog_post, block_data)
        blog_posts.append(blog_post)
    return blog_posts


def fetch_blog_posts():
    querystring = {'token': current_app.config['STORYBLOK_TOKEN'], 'cv': current_app.config['TIMESTAMP']}
    r = requests.get(current_app.config['STORYBLOK_URL'], params=querystring)
    if r.status_code != 200:
        return None

    blog_posts = parse_response(r.json())
    return blog_posts
