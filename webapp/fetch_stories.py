import os
import json

import requests
from dotenv import load_dotenv
load_dotenv()

querystring = {'token': os.getenv('STORYBLOK_TOKEN'), 'cv': os.getenv('TIMESTAMP')}
r = requests.get(os.getenv('STORYBLOK_URL'), params=querystring)

print(r.status_code)
print(json.dumps(r.json(), sort_keys=True, indent=4))
