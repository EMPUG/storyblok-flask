# Storyblok Flask Demo

Storyblok is a headless CMS service, which allows one to define content schemas,
create content, and retrieve that content in JSON format using the Storyblok REST API.

Storyblok is language agnostic. The
[Storyblok Content Delivery API](https://www.storyblok.com/docs/api/content-delivery)
documentation provides examples in several languages including Python.

More information about Storyblok can be found on the [Storyblok home page](https://www.storyblok.com).


### Packages

The project uses the following Python packages:

- the [Flask](https://palletsprojects.com/p/flask/) web framework
- [requests](https://requests.readthedocs.io/en/master/) to make the http GET request
  to retrieve the data from Storyblok
- [Flask-Mikasa](https://flask-misaka.readthedocs.io/en/latest/) for rendering markdown
- [aniso8601](https://pypi.org/project/aniso8601/) for converting date strings
  in ISO 8601 format to date objects
- [python-dotenv](https://pypi.org/project/python-dotenv/) which makes it easy
  to set environment variables during development

### Installation

1. Navigate to a directory somewhere where you keep your software projects:

        cd projects

2. Clone the storyblok-flask repository:

        git clone https://gitlab.com/EMPUG/storyblok-flask.git
        
3. Navigate to the new `storyblok-flask` directory which contains the repository.

        cd storyblok-flask

4. Create a Python 3 virtual environment called `env`:

        python3 -m venv env
        
5. Activate the environment:

        source env/bin/activate
        
6. Install required packages:

        pip install -r requirements.txt

7. Update the `webapp/.env` file with a valid Storyblok token.

### Retrieving data from the Storyblok REST API

1. Activate the virtual environment, if not already active:

        cd storyblok-flask
        source env/bin/activate
        
2. Change to the `webapp` folder:

        cd webapp

3. Execute the command:

        python fetch_stories.py

### Launching the Flask website

1. Activate the virtual environment, if not already active:

        cd storyblok-flask
        source env/bin/activate
        
2. Change to the `webapp` folder:

        cd webapp

3. Launch the Flask application:

        flask run
