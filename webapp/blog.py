import os

from flask import Flask, render_template
from flask_misaka import Misaka

from .storyblok import fetch_blog_posts

md = Misaka()
app = Flask(__name__)
md.init_app(app)

app.config['STORYBLOK_URL'] = os.getenv('STORYBLOK_URL')
app.config['STORYBLOK_TOKEN'] = os.getenv('STORYBLOK_TOKEN')
app.config['TIMESTAMP'] = os.getenv('TIMESTAMP')


@app.route('/')
def blog_page():
    blot_posts = fetch_blog_posts()
    return render_template('blog.html', blog_posts=blot_posts)
